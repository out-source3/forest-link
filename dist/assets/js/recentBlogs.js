$(document).ready(function () {
  renderRecentBlogs();
});

function renderRecentBlogs() {
  html = "";
  const content = blogs.map((blog, index) => {
    if (index >= 2 && index < 6) {
      html +=
        `<div class="widgets-list-item mb-4">` +
        `<div class="widgets-list-img">` +
        `<a href="${blog.id}"><img style="height: 67px" class="img-full" src="../${blog.thumbnail}" alt="Blog Images" /></a>` +
        `</div>` +
        `<div class="widgets-list-content">` +
        `<div class="widgets-meta"><ul><li class="date">${blog.date}</li></ul></div>` +
        `<h2 class="title mb-0"><a href="${blog.id}">${blog.title}</a></h2></div></div>`;
      return html;
    }
  });

  document.getElementById("recentBlogs").innerHTML = html;
}
