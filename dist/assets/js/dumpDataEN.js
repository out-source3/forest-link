const blogs = [
  {
    id: "social-change-makers-online.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-scomh.jpg",
    title: "Social ChangeMakers ONLINE – Mental Health",
    content:
      "Social Changemakers Network, an exclusive “Do Good.Feel Good.Live Good” [...]",
    author: "Author",
    date: "29 May",
  },
  {
    id: "activities-for-world-environment-day.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-afwed.jpg",
    title: "Activities for World Environment Day",
    content:
      "From June 5-7 2020, Forest Link has a small corner offering trees called “My garden” [...]",
    author: "Author",
    date: "5 Jun",
  },
  {
    id: "a-new-friend-of-forest-link.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-anfofl.jpg",
    title: "A new friend of Forest Link",
    content:
      "Each of us has an impact on our Mother Earth (both good and bad side). We have not many good news from the [...]",
    author: "Author",
    date: "11 Mar",
  },
  {
    id: "planting-trees-with-forest-link.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-ptwfl.jpg",
    title: "Planting trees with Forest Link",
    content:
      "Forest Link often receive questions about how can trees grow with Forest Link? How long [...]",
    author: "Author",
    date: "14 Feb",
  },
  {
    id: "crs-program-with-ikea-vietnam.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-cpwiv.jpg",
    title: "CSR program with IKEA Vietnam",
    content:
      "Yesterday was a beautiful day! Yesterday, 7th December 2019, Forest Link had [...]",
    author: "Author",
    date: "8 Dec",
  },
  {
    id: "blue-swallows-program-2019.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-bsp2019.jpg",
    title: "Blue swallows program 2019",
    content:
      "On 25-26/10/2019, Forest Link participated in the event of Blue Swallows Gala Honouring Business [...]",
    author: "Author",
    date: "28 Oct",
  },
  {
    id: "the-last-tree-planting-activity-in-2019-in-phuyen.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-tltpai2ipy.jpg",
    title: "The last tree planting activity in 2019 in Phu Yen",
    content:
      "On November 30th, 2019, Forest Link has completed the third tree planting phase and is also [...]",
    author: "Author",
    date: "5 Dec",
  },
  {
    id: "pot-painting-fundraiser-with-bui-art-centre.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-ppfwbac.jpg",
    title: "Pot painting fundraiser with Bụi Art Centre",
    content:
      "In order to share concerns about the forest and the environment, Bụi Art Centre (Saigon) [...]",
    author: "Author",
    date: "16 Oct",
  },
];
