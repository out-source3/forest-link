let perPage = 6;
let currentPage = 1;
let start = 0;
let end = perPage;

const totalPages = Math.ceil(blogs.length / perPage);

const btnNext = document.querySelector(".btn-next");
const btnPrevious = document.querySelector(".btn-previous");

$(document).ready(function () {
  renderBlogs();
  renderPageNumber();
});

function getCurrentPage(currentPage) {
  start = (currentPage - 1) * perPage;
  end = currentPage * perPage;
  renderBlogs();
}
function renderBlogs() {
  html = "";
  const content = blogs.map((blog, index) => {
    if (index >= start && index < end) {
      html +=
        `<div class="col-md-6">` +
        `<div class="blog-item">` +
        `<div class="blog-content">` +
        `<div class="blog-meta"><div class="tag"><a class="date">${blog.date}</a></div></div>` +
        `<h2 class="title title-thumbnail"><a href="blog/${blog.id}">${blog.title}</a></h2>` +
        `<p class="short-desc short-desc-thumbnail mb-7">${blog.content}</p></div>` +
        `<div class="blog-img img-hover-effect">` +
        `<a href="blog/${blog.id}"><img class="img-full" src="${blog.thumbnail}" alt="Blog Image"/></a>` +
        `<div class="inner-btn-wrap">` +
        `<a class="inner-btn" href="blog/${blog.id}"><i class="pe-7s-link"> </i></a>` +
        `</div></div></div></div>`;
      return html;
    }
  });

  document.getElementById("blogs").innerHTML = html;
}

function renderPageNumber() {
  let html = "";
  html += `<li class="page-item active"><a href="#blogs" class="page-link">${1}</a></li>`;
  for (let i = 2; i <= totalPages; i++) {
    html += `<li class="page-item"><a href="#blogs" class="page-link">${i}</a></li>`;
  }
  document.getElementById("pageNumber").innerHTML = html;
  addEventPageNumber();
}

function addEventPageNumber() {
  const currentPageNumber = document.querySelectorAll(".page-number li");
  for (let i = 0; i < currentPageNumber.length; i++) {
    currentPageNumber[i].addEventListener("click", () => {
      let value = i + 1;
      currentPage = value;
      $(".page-number li").removeClass("active");
      currentPageNumber[i].classList.add("active");
      if (currentPage === totalPages) {
        $(".btn-previous").removeClass("btn-active");
        $(".btn-next").addClass("btn-active");
      } else if (currentPage == 1) {
        $(".btn-previous").addClass("btn-active");
        $(".btn-next").removeClass("btn-active");
      } else {
        $(".btn-next").removeClass("btn-active");
        $(".btn-previous").removeClass("btn-active");
      }
      getCurrentPage(currentPage);
    });
  }
}

btnNext.addEventListener("click", () => {
  currentPage++;
  if (currentPage > totalPages) {
    currentPage = totalPages;
    return;
  }
  if (currentPage === totalPages) {
    $(".btn-next").addClass("btn-active");
  }
  $(".btn-previous").removeClass("btn-active");
  $(".page-number li").removeClass("active");
  $(`.page-number li:eq(${currentPage - 1})`).addClass("active");
  getCurrentPage(currentPage);
});

btnPrevious.addEventListener("click", () => {
  currentPage--;
  if (currentPage <= 1) {
    currentPage = 1;
  }
  if (currentPage === 1) {
    $(".btn-previous").addClass("btn-active");
  }
  $(".btn-next").removeClass("btn-active");
  $(".page-number li").removeClass("active");
  $(`.page-number li:eq(${currentPage - 1})`).addClass("active");
  getCurrentPage(currentPage);
});
