const blogs = [
  {
    id: "chuyen-trong-cay.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-ctc.jpg",
    title: "Chuyện trồng cây",
    content:
      "Để trồng một cây xanh trung bình bạn mất 30 phút để đào hố, 5 phút đặt cây vào [...]",
    author: "Author",
    date: "25 Th5",
  },
  {
    id: "forest-link-co-ban-moi.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-anfofl.jpg",
    title: "Forest Link có bạn mới",
    content:
      "Mỗi chúng ta đều tạo tác động lên Mẹ Thiên Nhiên (cả tác động tốt và không tốt). 2020 [...]",
    author: "Author",
    date: "11 Th3",
  },
  {
    id: "trong-cay-cung-forest-link.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-ptwfl.jpg",
    title: "Trồng cây cùng Forest Link",
    content:
      "Forest Link thường nhận được câu hỏi cây trồng với Forest Link thì có sống được không? Sau [...]",
    author: "Author",
    date: "14 Th2",
  },
  {
    id: "dot-trong-cay-cuoi-nam-2019-tai-phu-yen.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-tltpai2ipy.jpg",
    title: "Đợt trồng cây cuối năm 2019 tại Phú Yên",
    content:
      "Ngày 30.11.2019 vừa qua, nhà vườn Rừng Kết Nối đã hoàn thành đợt trồng cây thứ 3 và cũng là đợt cuối cùng [...]",
    author: "Author",
    date: "20 Th7",
  },
  {
    id: "vong-tron-chuoi.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-vtc.jpg",
    title: "Vòng tròn chuối",
    content:
      "Nếu sống ở vườn rừng có lẽ điều đầu tiên để thích nghi là bạn bắt đầu làm một vòng tròn chuối, điều này [...]",
    author: "Author",
    date: "30 Th10",
  },
  {
    id: "en-xanh-2019.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-bsp2019.jpg",
    title: "Én xanh 2019",
    content:
      "Ngày 25-26/10/2019, Forest Link đã có dịp tham dự Chương trình Tôn vinh sáng kiến kinh doanh vì mục tiêu [...]",
    author: "Author",
    date: "28 th10",
  },
  {
    id: "dot-trong-cay-dau-tien-tai-tram-co-daklak.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-dtcdtttcd.jpg",
    title: "Đợt trồng cây đầu tiên tại Trạm Cỏ, Daklak",
    content:
      "Chúng mình vừa trở về từ chuyến đi trồng rừng đầu tiên của Tháng 07, những ngày trồng rừng dưới cái nắng gắt [...]",
    author: "Author",
    date: "20 Th7",
  },
  {
    id: "tam-rung-tai-alba-welnnes-by-fusion-hue.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-trtawbfh.jpg",
    title: "Tắm rừng tại Alba Welnnes by Fusion Huế",
    content:
      "Nhà #ForestLink chân thành gửi lời cảm ơn đến những vị khách thân thương, đã cùng nhà đi và trải nghiệm [...]",
    author: "Author",
    date: "10 Th7",
  },
  {
    id: "hoat-dong-mung-ngay-moi-truong-the-gioi.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-afwed.jpg",
    title: "Hoạt động mừng Ngày môi trường thế giới",
    content:
      "Từ ngày 5-7/6/2020 Forest Link có 1 góc tặng cây nhỏ mang tên “Khu vườn của em” tại đường sách TPHCM [...]",
    author: "Author",
    date: "5 Th6",
  },
  {
    id: "tam-rung-cung-forest-link.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-trcfl.jpg",
    title: "Tắm rừng cùng Forest Link",
    content:
      "Bạn thân mến, sau thời gian dài thất hứa với các bạn về việc tổ chức chuyến tắm rừng đầu tiên thì  [...]",
    author: "Author",
    date: "4 Th6",
  },
  {
    id: "tour-trong-cay-thang-7-tai-phu-yen.html",
    thumbnail: "../../assets/images/blog/medium-size/blog-md-trct7tpy.jpg",
    title: "Tour trồng cây tháng 7 tại Phú Yên",
    content:
      "Forest Link cam kết sẽ sử dụng 51% lợi nhuận các tour tắm rừng, trồng cây… được sử dụng tái đầu tư cho [...]",
    author: "Author",
    date: "1 Th8",
  },
];
